const urls = require('./urls');

module.exports = function () {
  this.Given(/^я захожу на страницу аутентификации$/, function () {
    return browser.url(urls.loginUrl);
  });

  this.When(/^я оставляю в поле "([^"]*)" пустое значение "([^"]*)"$/, function (fieldName, value) {
    const input = browser.element(`input[name='${fieldName}']`);
    return input.setValue(value);
  });

  this.When(/^я ввожу в поле "([^"]*)" значение "([^"]*)"$/, function (fieldName, value) {
    const input = browser.element(`input[name='${fieldName}']`);
    input.waitForExist(5000);
    return input.setValue(value);
  });

  this.When(/^нажимаю на кнопку "([^"]*)"$/, function (text) {
    const button = browser.element(`button=${text}`);
    return button.click();
  });

  this.Then(/^я вижу сообщение с ошибкой/, function () {
    return browser.waitForExist('.errorMessage', 5000);
  });

  this.Then(/^я вижу в тулбаре приветствие$/, function () {
    return browser.waitForExist('#toolbar-welcome', 5000);
  });
};