export const baseUrl = 'http://localhost:3000';

module.exports = {
  loginUrl: baseUrl + '/login',
  addPlaceUrl: baseUrl + '/add-place'
};