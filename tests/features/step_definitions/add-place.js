const urls = require('./urls');

module.exports = function () {
  this.Given(/^я захожу на страницу добавления заведения$/, function () {
    return browser.url(urls.addPlaceUrl);
  });

  this.When(/^я ввожу в текстовое поле "([^"]*)" значение "([^"]*)"$/, function (fieldName, value) {
    const input = browser.element(`textarea[name='${fieldName}']`);
    input.waitForExist(5000);
    return input.setValue(value);
  });

  this.When(/^нажимаю на поле "([^"]*)"$/, function (text) {
    const field = browser.element(`#confirm-checkbox`);
    return field.click();
  });

  this.Then(/^я вижу третье превью$/, function () {
    return browser.waitForExist('#preview2', 5000);
  });
};