const express = require('express');
const moment = require('moment');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const upload = require('../middleware/upload');

const User = require('../models/User');
const Place = require('../models/Place');

const createRouter = () => {
  const router = express.Router();

  router.get('/', async (req, res) => {
    let places;
    try {
      places = await Place.find().populate('owner');
      res.send(places);
    } catch (error) {
      res.status(500).send({message: "Не удалось получить список заведений"});
    }
  });

  router.get('/:id', async (req, res) => {
    let place;
    try {
      place = await Place.findById(req.params.id).populate('owner');
      res.send(place);
    } catch (error) {
      res.status(500).send({message: "Не удалось получить детализацию по заведению"});
    }
  });

  router.post('/', [auth, upload.single('image')], (req, res) => {
    const placeData = {
      title: req.body.title,
      description: req.body.description,
      owner: req.user._id
    };

    if (req.file) {
      placeData.image = req.file.filename;
    } else {
      placeData.image = null;
    }

    const place = new Place(placeData);

    place.save()
      .then(place => res.send(place))
      .catch(error => res.status(400).send(error))
  });

  router.post('/add-photo/:id', [auth, upload.single('image')], async (req, res) => {
    let place;
    try {
      place = await Place.findById(req.params.id);

      if (!place) {
        return res.status(400).send({message: "Заведение не найдено"})
      }

      if (req.file) {
        place.photos.push(req.file.filename);
      }

      place.save()
        .then(place => res.send(place))
        .catch(error => res.status(400).send(error))
    } catch (error) {
      res.status(500).send({message: "Не удалось добавить фото"});
    }
  });

  router.post('/add-review/:id', auth, async (req, res) => {
    let place, user, review;

    try {
      place = await Place.findById(req.params.id);

      if (!place) {
        return res.status(400).send({message: "Заведение не найдено"})
      }

      user = await User.findById(req.user._id);

      if (!user) {
        return res.status(400).send({message: "Такой пользователь не найден"})
      }

      if (!req.body.text) {
        return res.status(400).send({message: "Отзыв не должен быть пустым"})
      }

      review = {
        user: user.username,
        text: req.body.text,
        rating: req.body.rating,
        datetime: moment().format()
      };

      place.reviews.push(review);

      place.save()
        .then(place => res.send(place))
        .catch(error => res.status(400).send(error))
    } catch (error) {
      res.status(500).send({message: "Не удалось добавить отзыв"});
    }
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    let place;
    try {
      place = await Place.findByIdAndDelete({_id: req.params.id});
      res.send(place);
    } catch (error) {
      res.status(500).send({message: "Не удалось удалить заведение"});
    }
  });

  router.delete('/remove-photo/:id/:name', [auth, permit('admin')], async (req, res) => {
    let place;
    try {
      place = await Place.findById(req.params.id);

      if (!place) {
        return res.status(400).send({message: "Не удалось найти заведение"})
      }

      const index = place.photos.findIndex(place => place === req.params.name);
      place.photos.splice(index, 1);

      place.save()
        .then(place => res.send(place))
        .catch(error => res.status(400).send(error))
    } catch (error) {
      res.status(500).send({message: "Не удалось удалить заведение"});
    }
  });

  router.delete('/remove-review/:id/:datetime', [auth, permit('admin')], async (req, res) => {
    let place;
    try {
      place = await Place.findById(req.params.id);

      if (!place) {
        return res.status(400).send({message: "Не удалось найти заведение"})
      }

      const index = place.reviews.findIndex(review => review.datetime === req.params.datetime);
      place.reviews.splice(index, 1);

      place.save()
        .then(place => res.send(place))
        .catch(error => res.status(400).send(error))
    } catch (error) {
      res.status(500).send({message: "Не удалось удалить заведение"});
    }
  });

  return router;
};

module.exports = createRouter;