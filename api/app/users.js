const express = require('express');
const User = require('../models/User');

const createRouter = () => {
  const router = express.Router();

  router.post('/register', (req, res) => {
    if (!req.body.username || req.body.username[0] === ' ')
      return res.status(400).send({message: "Недопустимое имя пользователя"});

    if (!req.body.password)
      return res.status(400).send({message: "Пароль не должен быть пустым"});

    if (req.body.password !== req.body.confirmPassword)
      return res.status(400).send({message: "Пароли не совпадают"});

    const user = new User({
      username: req.body.username,
      password: req.body.password
    });

    user.save()
      .then(user => res.send(user))
      .catch(error => res.status(400).send(error))
  });

  router.post('/sessions', async (req, res) => {
    let user;
    try {
      user = await User.findOne({username: req.body.username});
    }
    catch (error) {
      res.status(400).send({message: error})
    }

    if (!user) {
      return res.status(400).send({message: 'Имя пользователя или пароль неправильные!'});
    }
    let isMatch;
    try {
      isMatch = await user.checkPassword(req.body.password);
    }
    catch (error) {
      res.status(400).send({message: error})

    }

    if (!isMatch) {
      return res.status(400).send({message: 'Имя пользователя или пароль неправильные!'});
    }

    const token = user.generateToken();
    user.token = token;
    try {
      await user.save();
    }
    catch (error) {
      res.status(400).send({message: error})

    }

    return res.send({message: 'Пользователь и пароль правильные!', user, token});
  });


  router.delete('/sessions', async (req, res) => {
    const token = req.get('x-access-token');
    const success = {message: 'Вы успешно вышли из приложения!'};

    if (!token) return res.send(success);
    let user;
    try {
      user = await User.findOne({token});
    }
    catch (error) {
      res.status(400).send({message: error})
    }

    if (!user) return res.send(success);

    user.generateToken();
    try {
      await user.save();
    }
    catch (error) {
      res.status(400).send({message: error})
    }

    return res.send(success);
  });

  return router;
};

module.exports = createRouter;