const mongoose = require("mongoose");
const config = require("./config");

const User = require('./models/User');
const Place = require('./models/Place');

mongoose.connect(config.db.url);

const db = mongoose.connection;

console.log('##### start');

db.once("open", async () => {
  try {
    await db.dropDatabase();
  } catch (e) {
    console.log("Database were not present, skipping drop...");
  }

  const [user1, user2] = await User.create([
    {
      username: "admin",
      password: "111",
      role: "admin"
    },
    {
      username: "user",
      password: "111"
    }
  ]);

  await Place.create([
    {
      title: "Бар Базар",
      description: "Барчик-базарчик",
      owner: user1._id,
      image: "bar.png",
      photos: ["bar.png", "bar.png", "bar.png"],
      reviews: [
        {
          datetime: "2018-09-08T19:51:13+06:00",
          rating: {
            food: 3,
            interior: 2,
            service: 4
          },
          text: "Место ништячок",
          user: "Вася Пупкин"
        },
        {
          datetime: "2018-09-08T19:54:13+06:00",
          rating: {
            food: 4,
            interior: 3,
            service: 3
          },
          text: "Место ништячок",
          user: "Петя Васильков"
        }
      ]
    },
    {
      title: "Кафе Огонек",
      description: "Че, дружок, залетай на огонек!",
      owner: user2._id,
      image: "cafe.jpg",
      photos: ["cafe.jpg", "cafe.jpg", "cafe.jpg"],
      reviews: [
        {
          datetime: "2018-09-08T19:53:13+06:00",
          rating: {
            food: 2,
            interior: 3,
            service: 4
          },
          text: "Место ништячок",
          user: "Вася Пупкин"
        },
        {
          datetime: "2018-09-08T19:56:13+06:00",
          rating: {
            food: 3,
            interior: 4,
            service: 4
          },
          text: "Место ништячок",
          user: "Петя Васильков"
        }
      ]
    }
  ]);

  db.close();

  console.log('##### finish')
});