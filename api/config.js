const path = require('path');
const rootPath = __dirname;

module.exports = {
  db: {
    url: "mongodb://localhost:27017/places"
  },
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads/')
};