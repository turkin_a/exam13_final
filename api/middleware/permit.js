const User = require('../models/User');

const permit = (...roles) => {
  return async (req, res, next) => {
    if (!req.user) {
      return res.status(401).send({message: "Не аутентифицирован"});
    }

    const user = await User.findById(req.user._id);
    if (!roles.includes(user.role)) {
      return res.status(403).send({message: "Не авторизован"});
    }

    next();
  }
};

module.exports = permit;