const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  image: {
    type: String,
    // required: true
  },
  photos: {
    type: Array
  },
  reviews: {
    type: Array
  }
});

const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;