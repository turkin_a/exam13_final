import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Grid, PageHeader} from "react-bootstrap";

import FormElement from "../../components/UI/Form/FormElement";
import {clearUserErrors, registerUser} from "../../store/actions/users";

class Register extends Component {
  state = {
    username: '',
    password: '',
    confirmPassword: '',
    error: null
  };

  componentDidMount() {
    this.props.onClearUserErrors();
  }

  onChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value, error: null});
  };

  formSubmitHandler = event => {
    event.preventDefault();
    this.props.onClearUserErrors();

    if (this.state.username === '' || this.state.username[0] === ' ') {
      this.setState({error: 'Недопустимое имя пользователя'});
      return null;
    }

    if (this.state.password === '') {
      this.setState({error: 'Пароль не должен быть пустым'});
      return null;
    }

    if (this.state.password !== this.state.confirmPassword) {
      this.setState({error: 'Пароли не совпадают'});
      return null;
    }

    this.props.onRegisterUser(this.state);
  };

  render() {
    return (
      <Grid>
        <PageHeader>Регистрация пользователя</PageHeader>

        <Form horizontal>

          <FormElement
            propertyName="username"
            title="Имя пользователя"
            placeholder="Имя пользователя"
            type="text"
            value={this.state.username}
            changeHandler={this.onChangeHandler}
            required
          />

          <FormElement
            propertyName="password"
            title="Пароль"
            placeholder="Введите пароль"
            type="password"
            value={this.state.password}
            changeHandler={this.onChangeHandler}
          />

          <FormElement
            propertyName="confirmPassword"
            title="Подтвердите пароль"
            placeholder="Введите пароль еще раз"
            type="password"
            value={this.state.confirmPassword}
            changeHandler={this.onChangeHandler}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <p style={{fontSize: '18px', fontWeight: '600', color: 'red'}}>
                {this.state.error ? this.state.error : this.props.registerError}
              </p>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button onClick={this.formSubmitHandler} bsStyle="primary" type="submit">Зарегистрироваться</Button>
            </Col>
          </FormGroup>

        </Form>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  registerError: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  onClearUserErrors: () => dispatch(clearUserErrors()),
  onRegisterUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);