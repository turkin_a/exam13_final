import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Grid, PageHeader} from "react-bootstrap";

import FormElement from "../../components/UI/Form/FormElement";
import {clearUserErrors, loginUser} from "../../store/actions/users";

class Register extends Component {
  state = {
    username: '',
    password: '',
    error: null
  };

  componentDidMount() {
    this.props.onClearUserErrors();
  }

  onChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value, error: null});
  };

  formSubmitHandler = event => {
    event.preventDefault();
    this.props.onClearUserErrors();

    if (this.state.username === '') {
      this.setState({error: 'Введите имя пользователя'});
      return null;
    }

    if (this.state.password === '') {
      this.setState({error: 'Введите пароль'});
      return null;
    }

    this.props.onLoginUser(this.state);
  };

  render() {
    return (
      <Grid>
        <PageHeader>Авторизация пользователя</PageHeader>

        <Form horizontal>

          <FormElement
            propertyName="username"
            title="Имя пользователя"
            placeholder="Имя пользователя"
            type="text"
            value={this.state.username}
            changeHandler={this.onChangeHandler}
            required
          />

          <FormElement
            propertyName="password"
            title="Пароль"
            placeholder="Введите пароль"
            type="password"
            value={this.state.password}
            changeHandler={this.onChangeHandler}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              {(this.state.error || this.props.loginError) ?
                <p className="errorMessage" style={{fontSize: '18px', fontWeight: '600', color: 'red'}}>
                  {this.state.error ? this.state.error : this.props.loginError}
                </p>
                : null
              }
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button onClick={this.formSubmitHandler} bsStyle="primary" type="submit">Войти</Button>
            </Col>
          </FormGroup>

        </Form>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  token: state.users.token,
  loginError: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
  onClearUserErrors: () => dispatch(clearUserErrors()),
  onLoginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);