import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Checkbox, Col, ControlLabel, Form, FormGroup, Grid, Image, PageHeader} from "react-bootstrap";

import FormElement from "../../components/UI/Form/FormElement";
import {addPlace} from "../../store/actions/places";

class PlaceEditor extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    imageBase64: null,
    confirmed: false,
    error: null
  };

  onChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value, error: null});
  };

  fileChangeHandler = async event => {
    const fReader = new FileReader();

    fReader.readAsDataURL(event.target.files[0]);
    fReader.onloadend = e => {
      this.setState({imageBase64: e.target.result});
    };

    this.setState({[event.target.name]: event.target.files[0]});
  };

  checkboxChangeHandler = () => {
    this.setState(prevState => ({
      confirmed: !prevState.confirmed
    }));
  };

  formSubmitHandler = event => {
    event.preventDefault();

    if (!this.state.confirmed) return null;

    const innerState = {...this.state};
    delete innerState.imageBase64;
    delete innerState.error;

    const formData = new FormData();
    Object.keys(innerState).forEach(key => {
      formData.append(key, innerState[key]);
    });

    this.props.onAddPlace(formData);
    this.setState({image: '', imageBase64: null})
  };

  render() {
    return (
      <Grid>
        <PageHeader>Добавить заведение</PageHeader>

        <Form horizontal>

          <FormElement
            propertyName="title"
            title="Название"
            placeholder="Название заведения"
            type="text"
            value={this.state.title}
            changeHandler={this.onChangeHandler}
            required
          />

          <FormElement
            propertyName="description"
            title="Описание"
            placeholder="Описание заведения"
            type="textarea"
            value={this.state.description}
            changeHandler={this.onChangeHandler}
          />

          {this.state.imageBase64 &&
          <FormGroup>
            <Col sm={2} componentClass={ControlLabel}>
              Просмотр изображения
            </Col>
            <Col sm={8}>
              <Image
                src={this.state.imageBase64}
                style={{width: '250px', height: 'auto'}}
                thumbnail
              />
            </Col>
          </FormGroup>}

          <FormElement
            propertyName="image"
            title="Изображение"
            type="file"
            changeHandler={this.fileChangeHandler}
          />

          <FormGroup>
            <Col sm={4} componentClass={ControlLabel}>
              Правила по размещению контента на данном сайте. Обязательны к прочтению!
            </Col>
            <Col sm={8}>
              <Checkbox
                id="confirm-checkbox"
                checked={this.state.confirmed}
                onChange={this.checkboxChangeHandler}
              >
                Я согласен с правилами
              </Checkbox>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <p style={{fontSize: '18px', fontWeight: '600', color: 'red'}}>
                {this.state.error ? this.state.error : this.props.placeError}
              </p>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button
                onClick={this.formSubmitHandler}
                bsStyle="primary"
                type="submit"
                disabled={!this.state.confirmed}
              >
                Создать
              </Button>
            </Col>
          </FormGroup>

        </Form>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  onAddPlace: placeData => dispatch(addPlace(placeData))
});

export default connect(mapStateToProps, mapDispatchToProps)(PlaceEditor);