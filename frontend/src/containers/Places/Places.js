import React, {Component} from 'react';
import {connect} from "react-redux";
import {Col, Grid, PageHeader, Row} from "react-bootstrap";

import PlacePreview from "../../components/PlacePreview/PlacePreview";

import {fetchPlaces, removePlace} from "../../store/actions/places";

class Places extends Component {
  componentDidMount() {
    this.props.onFetchPlaces();
  }

  redirectToDetails = id => {
    this.props.history.push(`/place-details/${id}`);
  };

  removePlaceHandler = id => {
    this.props.onRemovePlace(id);
  };

  render() {
    return (
      <Grid>
        <PageHeader>Все заведения</PageHeader>

        <Row>
          {this.props.places.map((place, index) => (
            <Col key={place._id} md={4} sm={6} xs={12}>
              <PlacePreview
                index={index}
                place={place}
                user={this.props.user}
                details={this.redirectToDetails}
                remove={this.removePlaceHandler}
              />
            </Col>
          ))}
        </Row>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  places: state.places.places,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchPlaces: () => dispatch(fetchPlaces()),
  onRemovePlace: id => dispatch(removePlace(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Places);