import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, ControlLabel, Form, FormGroup, Glyphicon, Grid, Image, Panel, Row} from "react-bootstrap";
import moment from "moment/moment";
import Rating from 'react-rating';

import config from "../../config";

import {addPhoto, addReview, fetchPlaceDetails, removePhoto, removeReview} from "../../store/actions/places";
import FormElement from "../../components/UI/Form/FormElement";

class PlaceDetails extends Component {
  state = {
    review: '',
    food: 5,
    service: 5,
    interior: 5,
    image: '',
    imageBase64: '',
    summaryRating: {
      food: 0,
      service: 0,
      interior: 0,
      summary: 0
    }
  };

  componentDidMount() {
    this.props.onFetchPlaceDetails(this.props.match.params.id);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let summaryRating = {
      food: 0,
      service: 0,
      interior: 0,
      summary: 0
    };
    let counter = 0;

    const calculateRating = (value, count) => {
      return Math.round(value / count * 10) / 10;
    };

    if (nextProps.place) {
      if (nextProps.place.reviews) {
        nextProps.place.reviews.forEach(review => {
          if (review.rating) {
            counter++;
            summaryRating = {
              food: calculateRating((summaryRating.food + review.rating.food), counter),
              service: calculateRating((summaryRating.service + review.rating.service), counter),
              interior: calculateRating((summaryRating.interior + review.rating.interior), counter)
            }
          }
        });
      }

      if (counter > 0) {
        summaryRating.summary = calculateRating((summaryRating.food + summaryRating.service + summaryRating.interior), 3);
        return {...prevState, summaryRating};
      }
    }

    return {...prevState, summaryRating};
  }

  onChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value, error: null});
  };

  ratingChangeHandler = (ratingType, value) => {
    this.setState({[ratingType]: value, error: null});
  };

  fileChangeHandler = async event => {
    const fReader = new FileReader();

    fReader.readAsDataURL(event.target.files[0]);
    fReader.onloadend = e => {
      this.setState({imageBase64: e.target.result});
    };

    this.setState({[event.target.name]: event.target.files[0]});
  };

  reviewSubmitHandler = event => {
    event.preventDefault();

    if (!this.state.review) return null;

    const rating = {
      food: this.state.food,
      service: this.state.service,
      interior: this.state.interior
    };

    this.props.onAddReview(this.props.match.params.id, this.state.review, rating);
    this.setState({review: ''});
  };

  addFileSubmitHandler = event => {
    event.preventDefault();

    const innerState = {...this.state};
    delete innerState.imageBase64;

    const formData = new FormData();
    Object.keys(innerState).forEach(key => {
      formData.append(key, innerState[key]);
    });

    this.props.onAddPhoto(this.props.match.params.id, formData);
    this.fileInput.value = '';
    this.setState({imageBase64: ''});
  };

  removePhotoHandler = name => {
    this.props.onRemovePhoto(name, this.props.match.params.id);
  };

  removeReviewHandler = datetime => {
    this.props.onRemoveReview(datetime, this.props.match.params.id);
  };

  render() {
    if (!this.props.place) return null;

    return (
      <Grid>
        <Row>
          <Col sm={8}>
            <h1>{this.props.place.title}</h1>
            <p>{this.props.place.description}</p>
          </Col>
          <Col sm={4}>
            <Image
              style={{height: 'auto', width: '80%'}}
              src={this.props.place.image && config.apiUploads + this.props.place.image}
              rounded
            />
          </Col>
        </Row>

        {this.props.place.photos.length > 0 ?
          <Row style={{borderBottom: '1px solid silver', paddingTop: '20px', paddingBottom: '20px'}}>
            <h3>Галерея</h3>
            {this.props.place.photos.map((photo, index) => (
              <Col md={2} sm={4} xs={6} key={`photo${index}`}>
                <div style={{height: '150px'}}>
                  <Image
                    style={{width: '90%', height: '90%', objectFit: 'cover'}}
                    src={config.apiUploads + photo}
                    rounded
                  />
                </div>
                {(this.props.user && this.props.user.role === 'admin') ?
                  <p>
                    <Button onClick={() => this.removePhotoHandler(photo)}
                            bsStyle="danger"
                            type="submit"
                    >
                      Удалить
                    </Button>
                  </p>
                  : null
                }
              </Col>
            ))}
          </Row>
          : null
        }

        <Row style={{borderBottom: '1px solid silver', paddingTop: '20px', paddingBottom: '20px'}}>
          <h3>Рейтинг</h3>
          <Col sm={12}>
            <Col sm={2} componentClass={ControlLabel} style={{textAlign: 'right'}}>Суммарный:</Col>
            <Col sm={10}>
              <Rating
                style={{paddingLeft: '10px', paddingRight: '10px'}}
                initialRating={this.state.summaryRating.summary}
                readonly
              />
              <span>{this.state.summaryRating.summary}</span>
            </Col>
          </Col>
          <Col sm={12}>
            <Col sm={2} componentClass={ControlLabel} style={{textAlign: 'right'}}>Кухня:</Col>
            <Col sm={10}>
              <Rating
                style={{paddingLeft: '10px', paddingRight: '10px'}}
                initialRating={this.state.summaryRating.food}
                readonly
              />
              <span>{this.state.summaryRating.food}</span>
            </Col>
          </Col>
          <Col sm={12}>
            <Col sm={2} componentClass={ControlLabel} style={{textAlign: 'right'}}>Сервис:</Col>
            <Col sm={10}>
              <Rating
                style={{paddingLeft: '10px', paddingRight: '10px'}}
                initialRating={this.state.summaryRating.service}
                readonly
              />
              <span>{this.state.summaryRating.service}</span>
            </Col>
          </Col>
          <Col sm={12}>
            <Col sm={2} componentClass={ControlLabel} style={{textAlign: 'right'}}>Интерьер:</Col>
            <Col sm={10}>
              <Rating
                style={{paddingLeft: '10px', paddingRight: '10px'}}
                initialRating={this.state.summaryRating.interior}
                readonly
              />
              <span>{this.state.summaryRating.interior}</span>
            </Col>
          </Col>
        </Row>

        {this.props.user.token ?
          <Fragment>
            <Row style={{paddingTop: '20px'}}>
              <h3>Отзывы</h3>

              {this.props.place.reviews.map((review, index) => (
                <Panel key={`review${index}`}>
                  <Panel.Heading>
                    <Panel.Title componentClass="h5">
                      {(this.props.user && this.props.user.role === 'admin') ?
                        <span style={{marginRight: '20px', cursor: 'pointer'}}>
                          <Glyphicon
                            glyph={'remove'}
                            style={{color: 'red'}}
                            onClick={() => this.removeReviewHandler(review.datetime)}
                          />
                        </span>
                        : null
                      }
                      <strong>{review.user} </strong>
                      <span style={{color: '#646464', fontSize: '12px', fontStyle: 'italic', paddingLeft: '10px'}}>
                        {moment(review.datetime).format("YYYY-MM-DD HH:mm")}
                      </span>
                    </Panel.Title>
                  </Panel.Heading>
                  <Panel.Body>
                    <Col sm={12} style={{paddingBottom: '20px'}}>
                      {review.text}
                    </Col>
                    {review.rating ?
                      <Fragment>
                        <Col sm={4}>
                          <span>Кухня:</span>
                          <Rating
                            style={{paddingLeft: '10px', paddingRight: '10px'}}
                            initialRating={review.rating.food}
                            readonly
                          />
                          <span>{review.rating.food}</span>
                        </Col>
                        <Col sm={4}>
                          <span>Сервис:</span>
                          <Rating
                            style={{paddingLeft: '10px', paddingRight: '10px'}}
                            initialRating={review.rating.service}
                            readonly
                          />
                          <span>{review.rating.service}</span>
                        </Col>
                        <Col sm={4}>
                          <span>Интерьер:</span>
                          <Rating
                            style={{paddingLeft: '10px', paddingRight: '10px'}}
                            initialRating={review.rating.interior}
                            readonly
                          />
                          <span>{review.rating.interior}</span>
                        </Col>
                      </Fragment>
                    : null
                    }
                  </Panel.Body>
                </Panel>
              )).reverse()}
            </Row>

            <Row style={{borderBottom: '1px solid silver', paddingTop: '20px', paddingBottom: '20px'}}>
              <h3>Оставить отзыв</h3>

              <Form horizontal>

                <FormElement
                  propertyName="review"
                  title="Отзыв"
                  placeholder="Оставить отзыв"
                  type="textarea"
                  value={this.state.review}
                  changeHandler={this.onChangeHandler}
                />

                <FormGroup style={{textAlign: 'center'}}>
                  <Col sm={4}>
                    <p><strong>Кухня</strong></p>
                    <Rating
                      initialRating={this.state.food}
                      onChange={(value) => this.ratingChangeHandler('food', value)}
                    />
                  </Col>
                  <Col sm={4}>
                    <p><strong>Сервис</strong></p>
                    <Rating
                      initialRating={this.state.service}
                      onChange={(value) => this.ratingChangeHandler('service', value)}
                    />
                  </Col>
                  <Col sm={4}>
                    <p><strong>Интерьер</strong></p>
                    <Rating
                      initialRating={this.state.interior}
                      onChange={(value) => this.ratingChangeHandler('interior', value)}
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col smOffset={2} sm={10}>
                    <Button
                      onClick={this.reviewSubmitHandler}
                      bsStyle="primary"
                      type="submit"
                      disabled={!this.state.review}
                    >
                      Добавить
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            </Row>

            <Row style={{paddingTop: '20px', paddingBottom: '20px'}}>
              <h3>Добавить фото</h3>

              <Form horizontal>

                <FormElement
                  propertyName="image"
                  title="Фото"
                  type="file"
                  inputRef={ref => this.fileInput = ref}
                  changeHandler={this.fileChangeHandler}
                />

                {this.state.imageBase64 &&
                <FormGroup>
                  <Col sm={2} componentClass={ControlLabel}>
                    Просмотр фото
                  </Col>
                  <Col sm={8}>
                    <Image
                      src={this.state.imageBase64}
                      style={{width: '250px', height: 'auto'}}
                      thumbnail
                    />
                  </Col>
                </FormGroup>}

                <FormGroup>
                  <Col smOffset={2} sm={10}>
                    <Button
                      onClick={this.addFileSubmitHandler}
                      bsStyle="primary"
                      type="submit"
                      disabled={!this.state.imageBase64}
                    >
                      Добавить
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            </Row>
          </Fragment>
          : null
        }
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  place: state.places.placeDetails,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchPlaceDetails: id => dispatch(fetchPlaceDetails(id)),
  onAddReview: (id, text, rating) => dispatch(addReview(id, text, rating)),
  onAddPhoto: (id, data) => dispatch(addPhoto(id, data)),
  onRemovePhoto: (name, id) => dispatch(removePhoto(name, id)),
  onRemoveReview: (datetime, id) => dispatch(removeReview(datetime, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(PlaceDetails);