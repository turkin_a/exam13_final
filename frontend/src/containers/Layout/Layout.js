import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import Toolbar from "../../components/UI/Toolbar/Toolbar";

import {logoutUser} from "../../store/actions/users";

class Layout extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Toolbar
            user={this.props.user}
            logout={this.props.onLogoutUser}
          />
        </header>
        <main className="container">{this.props.children}</main>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onLogoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);