import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import './index.css';

import registerServiceWorker from './registerServiceWorker';
import App from './App';
import store, {history} from './store/configureStore';
import axios from './axios-api';

axios.interceptors.request.use(config => {
  try {
    config.headers['x-access-token'] = store.getState().users.token;
  } catch (e) {
    // do nothing
  }
  return config;
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();