import React from "react";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const Toolbar = ({user, logout}) => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <LinkContainer to="/">
            <a>Places</a>
          </LinkContainer>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      {user ?
        <Nav>
          <NavItem disabled id="toolbar-welcome">Привет, {user.username}</NavItem>
        </Nav>
        : null
      }
      <Navbar.Collapse>
        {user ?
          <Nav pullRight>
            <LinkContainer to="/add-place" exact>
              <NavItem>Добавить заведение</NavItem>
            </LinkContainer>
            <NavItem onClick={() => logout()}>Выйти</NavItem>
          </Nav>
          :
          <Nav pullRight>
            <LinkContainer to="/login" exact>
              <NavItem>Войти</NavItem>
            </LinkContainer>
            <LinkContainer to="/register" exact>
              <NavItem>Зарегистрироваться</NavItem>
            </LinkContainer>
          </Nav>
        }
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Toolbar;