import React from 'react';
import {Button, Glyphicon, Image, Panel} from "react-bootstrap";

import config from '../../config';

const PlacePreview = ({place, user, details, remove, index}) => {
  return (
    <Panel>
      <Panel.Heading className={'clearfix'} onClick={() => details(place._id)}>
        <div id={`preview${index}`}
          style={{
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            fontSize: '16px',
            fontWeight: 'bold',
            cursor: 'pointer'
          }}
        >
          {place.title}
        </div>
      </Panel.Heading>

      <Panel.Body className={'text-center'} style={{height: '200px'}}>
        <Image
          style={{height: '100%', width: 'auto', cursor: 'pointer'}}
          src={place.image && config.apiUploads + place.image}
          onClick={() => details(place._id)}
          rounded
        />
      </Panel.Body>

      <Panel.Footer className={'clearfix'}>
        <p>Владелец: <strong>{place.owner.username}</strong></p>
        <p>
          <Glyphicon glyph={'camera'}/>
          <strong> {place.photos.length} фото</strong>
        </p>
        {(user && user.role === 'admin') ?
          <p>
            <Button onClick={() => remove(place._id)}
              bsStyle="danger"
              type="submit"
            >
              Удалить
            </Button>
          </p>
          : null
        }
      </Panel.Footer>
    </Panel>
  )
};

export default PlacePreview;