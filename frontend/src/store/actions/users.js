import axios from "../../axios-api";
import {push} from "react-router-redux";

import {
  CLEAR_USER_ERRORS,
  LOGIN_USER_FAILURE,
  LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGOUT_USER_FAILURE, LOGOUT_USER_REQUEST, LOGOUT_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS
} from "./actionTypes";

export const clearUserErrors = () => {
  return {type: CLEAR_USER_ERRORS};
};

const registerUserRequest = () => {
  return {type: REGISTER_USER_REQUEST};
};

const registerUserSuccess = user => {
  return {type: REGISTER_USER_SUCCESS, user};
};

const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
  return dispatch => {
    dispatch(registerUserRequest());
    return axios.post('/users/register', userData).then(
      response => {
        dispatch(registerUserSuccess(response.data));
        dispatch(loginUser(userData));
        dispatch(push('/'));
      },
      error => {
        dispatch(registerUserFailure(error.response.data))
      }
    )
  }
};

const loginUserRequest = () => {
  return {type: LOGIN_USER_REQUEST};
};

const loginUserSuccess = data => {
  return {type: LOGIN_USER_SUCCESS, data};
};

const loginUserFailure = error => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
  return dispatch => {
    dispatch(loginUserRequest());
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        dispatch(push('/'));
      },
      error => {
        dispatch(loginUserFailure(error.response.data))
      }
    )
  }
};

const logoutUserRequest = () => {
  return {type: LOGOUT_USER_REQUEST};
};

const logoutUserSuccess = user => {
  return {type: LOGOUT_USER_SUCCESS, user};
};

const logoutUserFailure = error => {
  return {type: LOGOUT_USER_FAILURE, error};
};

export const logoutUser = () => {
  return dispatch => {
    dispatch(logoutUserRequest());
    return axios.delete('/users/sessions').then(
      response => {
        dispatch(logoutUserSuccess(response.data))
      },
      error => {
        dispatch(logoutUserFailure(error))
      }
    )
  }
};