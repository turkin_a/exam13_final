import axios from "../../axios-api";
import {push} from "react-router-redux";

import {
  ADD_PHOTO_FAILURE, ADD_PHOTO_REQUEST, ADD_PHOTO_SUCCESS, ADD_PLACE_FAILURE, ADD_PLACE_REQUEST, ADD_PLACE_SUCCESS,
  ADD_REVIEW_FAILURE, ADD_REVIEW_REQUEST, ADD_REVIEW_SUCCESS, FETCH_PLACE_DETAILS_FAILURE, FETCH_PLACE_DETAILS_REQUEST,
  FETCH_PLACE_DETAILS_SUCCESS, FETCH_PLACES_FAILURE, FETCH_PLACES_REQUEST, FETCH_PLACES_SUCCESS, REMOVE_PHOTO_FAILURE,
  REMOVE_PHOTO_REQUEST, REMOVE_PHOTO_SUCCESS, REMOVE_PLACE_FAILURE, REMOVE_PLACE_REQUEST, REMOVE_PLACE_SUCCESS,
  REMOVE_REVIEW_FAILURE, REMOVE_REVIEW_REQUEST, REMOVE_REVIEW_SUCCESS
} from "./actionTypes";

const addPlaceRequest = () => {
  return {type: ADD_PLACE_REQUEST};
};

const addPlaceSuccess = () => {
  return {type: ADD_PLACE_SUCCESS};
};

const addPlaceFailure = error => {
  return {type: ADD_PLACE_FAILURE, error};
};

export const addPlace = placeData => {
  return dispatch => {
    dispatch(addPlaceRequest());
    return axios.post('/places', placeData).then(
      response => {
        dispatch(addPlaceSuccess());
        dispatch(push('/'));
      },
      error => {
        dispatch(addPlaceFailure(error.response.data))
      }
    )
  }
};

const addPhotoRequest = () => {
  return {type: ADD_PHOTO_REQUEST};
};

const addPhotoSuccess = () => {
  return {type: ADD_PHOTO_SUCCESS};
};

const addPhotoFailure = error => {
  return {type: ADD_PHOTO_FAILURE, error};
};

export const addPhoto = (id, data) => {
  return dispatch => {
    dispatch(addPhotoRequest());
    return axios.post(`/places/add-photo/${id}`, data).then(
      response => {
        dispatch(addPhotoSuccess());
        dispatch(fetchPlaceDetails(id));
      },
      error => {
        dispatch(addPhotoFailure(error.response.data))
      }
    )
  }
};

const addReviewRequest = () => {
  return {type: ADD_REVIEW_REQUEST};
};

const addReviewSuccess = () => {
  return {type: ADD_REVIEW_SUCCESS};
};

const addReviewFailure = error => {
  return {type: ADD_REVIEW_FAILURE, error};
};

export const addReview = (id, text, rating) => {
  return dispatch => {
    dispatch(addReviewRequest());
    return axios.post(`/places/add-review/${id}`, {text, rating}).then(
      response => {
        dispatch(addReviewSuccess());
        dispatch(fetchPlaceDetails(id));
      },
      error => {
        dispatch(addReviewFailure(error.response.data))
      }
    )
  }
};

const fetchPlacesRequest = () => {
  return {type: FETCH_PLACES_REQUEST};
};

const fetchPlacesSuccess = places => {
  return {type: FETCH_PLACES_SUCCESS, places};
};

const fetchPlacesFailure = error => {
  return {type: FETCH_PLACES_FAILURE, error};
};

export const fetchPlaces = () => {
  return dispatch => {
    dispatch(fetchPlacesRequest());
    return axios.get('/places').then(
      response => {
        dispatch(fetchPlacesSuccess(response.data));
      },
      error => {
        dispatch(fetchPlacesFailure(error.response.data))
      }
    )
  }
};

const fetchPlaceDetailsRequest = () => {
  return {type: FETCH_PLACE_DETAILS_REQUEST};
};

const fetchPlaceDetailsSuccess = place => {
  return {type: FETCH_PLACE_DETAILS_SUCCESS, place};
};

const fetchPlaceDetailsFailure = error => {
  return {type: FETCH_PLACE_DETAILS_FAILURE, error};
};

export const fetchPlaceDetails = id => {
  return dispatch => {
    dispatch(fetchPlaceDetailsRequest());
    return axios.get(`/places/${id}`).then(
      response => {
        dispatch(fetchPlaceDetailsSuccess(response.data));
      },
      error => {
        dispatch(fetchPlaceDetailsFailure(error.response.data))
      }
    )
  }
};

const removePlaceRequest = () => {
  return {type: REMOVE_PLACE_REQUEST};
};

const removePlaceSuccess = () => {
  return {type: REMOVE_PLACE_SUCCESS};
};

const removePlaceFailure = error => {
  return {type: REMOVE_PLACE_FAILURE, error};
};

export const removePlace = id => {
  return dispatch => {
    dispatch(removePlaceRequest());
    return axios.delete(`/places/${id}`).then(
      response => {
        dispatch(removePlaceSuccess());
        dispatch(fetchPlaces())
      },
      error => {
        dispatch(removePlaceFailure(error.response.data))
      }
    )
  }
};

const removePhotoRequest = () => {
  return {type: REMOVE_PHOTO_REQUEST};
};

const removePhotoSuccess = () => {
  return {type: REMOVE_PHOTO_SUCCESS};
};

const removePhotoFailure = error => {
  return {type: REMOVE_PHOTO_FAILURE, error};
};

export const removePhoto = (name, id) => {
  return dispatch => {
    dispatch(removePhotoRequest());
    return axios.delete(`/places/remove-photo/${id}/${name}`).then(
      response => {
        dispatch(removePhotoSuccess());
        dispatch(fetchPlaceDetails(id))
      },
      error => {
        dispatch(removePhotoFailure(error.response.data))
      }
    )
  }
};

const removeReviewRequest = () => {
  return {type: REMOVE_REVIEW_REQUEST};
};

const removeReviewSuccess = () => {
  return {type: REMOVE_REVIEW_SUCCESS};
};

const removeReviewFailure = error => {
  return {type: REMOVE_REVIEW_FAILURE, error};
};

export const removeReview = (datetime, id) => {
  return dispatch => {
    dispatch(removeReviewRequest());
    return axios.delete(`/places/remove-review/${id}/${datetime}`).then(
      response => {
        dispatch(removeReviewSuccess());
        dispatch(fetchPlaceDetails(id))
      },
      error => {
        dispatch(removeReviewFailure(error.response.data))
      }
    )
  }
};
