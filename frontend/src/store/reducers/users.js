import {
  CLEAR_USER_ERRORS,
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER_FAILURE, LOGOUT_USER_SUCCESS, REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  token: '',
  user: null,
  registerError: null,
  loginError: null,
  logoutError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_USER_ERRORS:
      return {...state, registerError: null, loginError: null, logoutError: null};
    case REGISTER_USER_SUCCESS:
      return {...state, registerError: null};
    case REGISTER_USER_FAILURE:
      return {...state, registerError: action.error.message};
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.data.user, token: action.data.token, loginError: null};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.error.message};
    case LOGOUT_USER_SUCCESS:
      return {...state, user: null, logoutError: null};
    case LOGOUT_USER_FAILURE:
      return {...state, logoutError: action.error.message};
    default:
      return state;
  }
};

export default reducer;