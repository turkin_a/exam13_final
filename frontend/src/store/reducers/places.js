import {
  ADD_PHOTO_FAILURE, ADD_PHOTO_REQUEST, ADD_PHOTO_SUCCESS, ADD_PLACE_FAILURE, ADD_PLACE_REQUEST, ADD_PLACE_SUCCESS,
  ADD_REVIEW_FAILURE, ADD_REVIEW_REQUEST, ADD_REVIEW_SUCCESS, FETCH_PLACE_DETAILS_FAILURE, FETCH_PLACE_DETAILS_REQUEST,
  FETCH_PLACE_DETAILS_SUCCESS, FETCH_PLACES_FAILURE, FETCH_PLACES_REQUEST, FETCH_PLACES_SUCCESS, REMOVE_PHOTO_FAILURE,
  REMOVE_PHOTO_REQUEST, REMOVE_PHOTO_SUCCESS, REMOVE_PLACE_FAILURE, REMOVE_PLACE_REQUEST, REMOVE_PLACE_SUCCESS,
  REMOVE_REVIEW_FAILURE, REMOVE_REVIEW_REQUEST, REMOVE_REVIEW_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  places: [],
  reviews: [],
  placeDetails: null,
  photoAdded: true,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PLACE_REQUEST:
      return {...state, error: null};
    case ADD_PLACE_SUCCESS:
      return {...state, error: null};
    case ADD_PLACE_FAILURE:
      return {...state, error: action.error.message};
    case ADD_PHOTO_REQUEST:
      return {...state, photoAdded: false, error: null};
    case ADD_PHOTO_SUCCESS:
      return {...state, photoAdded: true, error: null};
    case ADD_PHOTO_FAILURE:
      return {...state, photoAdded: false, error: action.error.message};
    case ADD_REVIEW_REQUEST:
      return {...state, error: null};
    case ADD_REVIEW_SUCCESS:
      return {...state, error: null};
    case ADD_REVIEW_FAILURE:
      return {...state, error: action.error.message};
    case FETCH_PLACES_REQUEST:
      return {...state, error: null};
    case FETCH_PLACES_SUCCESS:
      return {...state, places: action.places, error: null};
    case FETCH_PLACES_FAILURE:
      return {...state, error: action.error.message};
    case FETCH_PLACE_DETAILS_REQUEST:
      return {...state, error: null};
    case FETCH_PLACE_DETAILS_SUCCESS:
      return {...state, placeDetails: action.place, error: null};
    case FETCH_PLACE_DETAILS_FAILURE:
      return {...state, error: action.error.message};
    case REMOVE_PLACE_REQUEST:
      return {...state, error: null};
    case REMOVE_PLACE_SUCCESS:
      return {...state, error: null};
    case REMOVE_PLACE_FAILURE:
      return {...state, error: action.error.message};
    case REMOVE_PHOTO_REQUEST:
      return {...state, error: null};
    case REMOVE_PHOTO_SUCCESS:
      return {...state, error: null};
    case REMOVE_PHOTO_FAILURE:
      return {...state, error: action.error.message};
    case REMOVE_REVIEW_REQUEST:
      return {...state, error: null};
    case REMOVE_REVIEW_SUCCESS:
      return {...state, error: null};
    case REMOVE_REVIEW_FAILURE:
      return {...state, error: action.error.message};
    default:
      return state;
  }
};

export default reducer;