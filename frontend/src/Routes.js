import React from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Places from "./containers/Places/Places";
import PlaceEditor from "./containers/Places/PlaceEditor";
import PlaceDetails from "./containers/Places/PlaceDetails";

const ProtectedRoute = ({isAllowed, ...props}) =>
  isAllowed ? <Route {...props} /> : <Redirect to="/login"/>;

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={Places}/>
      <Route path="/place-details/:id" component={PlaceDetails}/>

      <ProtectedRoute
        isAllowed={user}
        path="/add-place"
        exact
        component={PlaceEditor}
      />

      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
      <Route render={() => <h1 style={{textAlign: "center"}}>Page not found</h1>}/>
    </Switch>
  );
};

const mapStateToProps = state => {
  return {
    user: state.users.user
  };
};

export default withRouter(connect(mapStateToProps)(Routes));